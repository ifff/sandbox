# Quick reminder about Makefiles:
# $< = 1st prereq listed
# $^ = all prereqs
# $@ = name of target
# Rules are in form of:
# ---------------------
# target: prereqs
# \t	command

CC = gcc
CFLAGS = -Wall
LIBS = -lSDL2
SOURCES = src/*.c src/renderer/*.c src/world/*.c src/pixel/*.c src/graphics/*.c src/rand/*.c
OBJECTS = $(SOURCES:.c=.o)
EXEC = bin/sandbox

$(EXEC): $(SOURCES)
	$(CC) $(CFLAGS) $(LIBS) $^ -o $@

debug: $(SOURCES)
	$(CC) $(CFLAGS) $(LIBS) $^ -g -o $(EXEC)

clean:
	rm -f $(EXEC) src/*.o
