with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "minecraft-2";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
