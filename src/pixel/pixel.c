#include "../rand/rand.h"
#include "pixel.h"

// allocates a new pixel
pixel* new_pixel(uint32_t flags, rgba_color color, uint32_t density, float temp) {
  pixel* p = malloc(sizeof(pixel));
  p->flags = flags;
  // the color may need to be randomized
  p->color = (flags & PX_RCOLOR)? randomize_color(color) : color;
  p->density = density;
  p->temp = temp;

  return p;
}

// allocates a new empty pixel
pixel* new_empty_pixel() {
  return pixel_from_id(ID_EMPTY);
}

// like `new_pixel', but only takes a pixel species id
pixel* pixel_from_id(uint8_t id) {
  pixel* p = new_pixel(0, 0, 0, 0.0f);
  transmute_pixel(p, id);
  
  return p;
}

// gives a pixel new characteristics based on the given species id
void transmute_pixel(pixel* p, uint8_t id) {
  p->id = id;

  switch (id) {
  case ID_EMPTY:
    p->flags = PX_EMPTY;
    p->color = 0xff;
    p->density = 0;
    p->temp = 0.0f;
    break;

  case ID_SAND:
    p->flags = PX_FALLS|PX_RCOLOR;
    p->color = 0xfcf9cfff;
    p->density = 3;
    p->temp = 0.0f;
    break;

  case ID_WATER:
    p->flags = PX_FALLS|PX_FLOWS;
    p->color = 0x22d0f7ff;
    p->density = 2;
    p->temp = 0.0f;
    break;

  case ID_ICE:
    p->flags = PX_FALLS;
    p->color = 0xddddffff;
    p->density = 1;
    p->temp = -1.0f;
    break;
  }  
}

// free a pixel
void free_pixel(pixel* p) {
  free(p);
}

// modify the brightness of a color by increasing or decreasing its brightness
// by a random amount
rgba_color randomize_color(rgba_color color) {
  int n = 10000;
  int mod = n - rand_int(5);
  int r = ((color >> 24) * mod / n) & 0xff;
  int g = ((color >> 16) * mod / n) & 0xff;
  int b = ((color >>  8) * mod / n) & 0xff;
  int a = color & 0xff;
  // calculate the modified color
  int modified_color = r;
  modified_color = (modified_color << 8) | g;
  modified_color = (modified_color << 8) | b;
  modified_color = (modified_color << 8) | a;

  return (rgba_color)modified_color;
}
