#ifndef PIX_PIXEL_H
#define PIX_PIXEL_H

#include <stdint.h>
#include <stdlib.h>
#include "../includes.h"

// bit fields!!!!!!!!!!!!!
#define PX_EMPTY    (1 << 0) // "empty" space
#define PX_FALLS    (1 << 1) // falls according to gravity
#define PX_FLOATS   (1 << 2) // floats upward (eg smoke/steam)
// #define PX_SHINY    (1 << 3) // dynamically sparkles
#define PX_RCOLOR   (1 << 4) // initialized with a slightly random color
#define PX_FINITE   (1 << 5) // disappears after a few seconds via randomness
#define PX_DISPLACEABLE (1 << 6) // can be displaced by another pixel
#define PX_FLOWS    (1 << 7) // flows like a liquid

#define ID_EMPTY 0
#define ID_SAND 1
#define ID_WATER 2
#define ID_ICE 3

typedef uint32_t rgba_color;

typedef struct {
  uint32_t flags;
  rgba_color color;
  uint32_t density;
  float temp;
  uint8_t id;
} pixel;

pixel* new_pixel(uint32_t flags, rgba_color color, uint32_t density, float temp);
pixel* new_empty_pixel();
void free_pixel(pixel* p);
rgba_color randomize_color(rgba_color color);
pixel* pixel_from_id(uint8_t id);
void transmute_pixel(pixel* p, uint8_t id);

#endif
