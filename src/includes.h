/**
 * This header file contains generic data/functionality that many
 * or all files will need to use.
 */

#ifndef INCLUDES_H
#define INCLUDES_H

// booleans!
#include <stdbool.h>

// are we in debugging mode? (for printfs)
#define DEBUG 1
// and if so, make sure to include stdio
#ifdef DEBUG
#include <stdio.h>
#endif

#define WORLD_WIDTH 175
#define WORLD_HEIGHT 100
#define PIXEL_SIZE 4
#define SCREEN_WIDTH (WORLD_WIDTH*PIXEL_SIZE)
#define SCREEN_HEIGHT (WORLD_HEIGHT*PIXEL_SIZE)

#endif
