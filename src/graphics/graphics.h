#ifndef GR_GRAPHICS_H
#define GR_GRAPHICS_H

#include "../includes.h"
#include "../renderer/renderer.h"
#include "../world/world.h"
#include "../pixel/pixel.h"

void draw_world(renderer_info* info, world* w);
void draw_pixel(renderer_info* info, pixel* p, int x, int y);
void set_color(renderer_info* info, rgba_color color);

#endif
