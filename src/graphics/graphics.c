#include "graphics.h"

void draw_world(renderer_info* info, world* w) {
  for (int x = 0; x < WORLD_WIDTH; x++) {
    for (int y = 0; y < WORLD_HEIGHT; y++) {
      pixel* p = get_pixel(w, x, y);
      draw_pixel(info, p, x, y);
    }
  }
}

void draw_pixel(renderer_info* info, pixel* p, int x, int y) {
  set_color(info, p->color);
  renderer_draw_rect(info, x*PIXEL_SIZE, y*PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE);
}

void set_color(renderer_info* info, rgba_color color) {
  // 0xrrggbbaa
  int r = (color >> 24) & 0xff;
  int g = (color >> 16) & 0xff;
  int b = (color >>  8) & 0xff;
  int a = color & 0xff;

  renderer_set_color(info, r, g, b, a);
}
