#include "rand.h"

// returns true in 1 out of n cases.
bool chance(int n) {
  return (rand() % n) == 0;
}

// returns an integer in the [0, n) range
int rand_int(int n) {
  return rand() % n;
}
