#ifndef RA_RAND_H
#define RA_RAND_H

#include <stdlib.h>
#include <stdbool.h>

bool chance(int n);
int rand_int(int n);

#endif
