#ifndef W_WORLD_H
#define W_WORLD_H

#include <stdlib.h>
#include "../includes.h"
#include "../pixel/pixel.h"
#include "../rand/rand.h"
#include "../renderer/event.h"

#define NUM_BRUSHES 4
#define ERASER_IDX 0

typedef unsigned char brush_num;

typedef struct {
  pixel* pixels[WORLD_WIDTH][WORLD_HEIGHT];
  pixel* new_pixels[WORLD_WIDTH][WORLD_HEIGHT];
  float heatmap[WORLD_WIDTH][WORLD_HEIGHT];
  int brush_x, brush_y;
  int brush_size;
  brush_num brush_idx;
  brush_num prev_brush_idx;
  pixel* brushes[NUM_BRUSHES];
} world;

world* new_world();
void destroy_world(world* w);

bool valid_coords(int x, int y);
bool is_empty(world* w, int x, int y);
pixel* get_pixel(world* w, int x, int y);
void set_pixel(world* w, int x, int y, pixel* p);
void set_from_brush(world* w, int x, int y);
void clear_pixel(world* w, int x, int y);
void draw_pixels(world* w);
pixel* clone_brush(world* w);
void flip(world* w);

void world_hdl_event(world* w, event* e);
void update_world(world* w);
void update_pixel(world* w, int x, int y);
bool move_pixel(world* w, int x, int y, int dx, int dy, pixel* p);
bool is_open_spot(world* w, uint32_t density, int x, int y);

float get_heatmap(world* w, int x, int y);
void set_heatmap(world* w, int x, int y, float h);
void update_heatmap(world* w);

#endif
