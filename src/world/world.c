#include <stdint.h>
#include "world.h"


// initialize a world with empty pixels and return a pointer to it
world* new_world() {
  world* w = malloc(sizeof(world));
  w->brush_idx = 1;
  w->brush_x = 0;
  w->brush_y = 0;
  w->brush_size = 0;

  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_HEIGHT; y++)
      w->heatmap[x][y] = 0.0f;

  // TODO: dynamically allocate pixels?
  
  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_HEIGHT; y++)
      clear_pixel(w, x, y);

  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_HEIGHT; y++)
      w->pixels[x][y] = new_empty_pixel();

  // set up the brushes
  w->brushes[ERASER_IDX] = pixel_from_id(ID_EMPTY);
  w->brushes[1] = pixel_from_id(ID_SAND);
  w->brushes[2] = pixel_from_id(ID_WATER);
  w->brushes[3] = pixel_from_id(ID_ICE);
  
#ifdef DEBUG
  printf("created world.\n");
#endif

  return w;
}

void destroy_world(world* w) {
  // TODO: free pixels?

  for (int i = 0; i < NUM_BRUSHES; i++)
    free_pixel(w->brushes[i]);

  free(w);

#ifdef DEBUG
  printf("destroyed world.\n");
#endif
}


// sets the world's pixels to its new pixels (if that explanation doesn't make sense,
// just think "double buffering")
void flip(world* w) {
  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_HEIGHT; y++)
      w->pixels[x][y] = w->new_pixels[x][y];
}

// determines whether or not coordinates are within
// the boundaries of the world's pixel array
bool valid_coords(int x, int y) {
  return (x >= 0 && x < WORLD_WIDTH) && (y >= 0 && y < WORLD_HEIGHT);
}

// determines whether or not a pixel cell is "empty"
// (coordinates are not validated)
bool is_empty(world* w, int x, int y) {
  pixel* p = get_pixel(w, x, y);
  return (p->flags & PX_EMPTY);
}

// get a pixel from the grid, given the coordinates
// (coordinates are not validated)
pixel* get_pixel(world* w, int x, int y) {
  return w->pixels[x][y];
}

// set the pixel at location (x, y)
// (coordinates are not validated)
void set_pixel(world* w, int x, int y, pixel* p) {
  w->new_pixels[x][y] = p;
}

// like `set_pixel', but automatically sets
// the pixel at (x, y) to a copy of the current brush,
// and also sets the heatmap temperature to that of the brush pixel
void set_from_brush(world* w, int x, int y) {
  pixel* p = clone_brush(w);
  set_pixel(w, x, y, p);
  set_heatmap(w, x, y, p->temp);
}

// set the pixel at (x, y) to empty
// (coordinates are not validated)
void clear_pixel(world* w, int x, int y) {
  set_pixel(w, x, y, new_empty_pixel());
}

// make a new pixel based on the current brush
pixel* clone_brush(world* w) {
  pixel* brush = w->brushes[w->brush_idx];
  pixel* p = new_pixel(brush->flags, brush->color, brush->density, brush->temp);
  return p;
}

// draw pixels centered at the brush, taking brush radius into account
void draw_pixels(world* w) {
  for (int i = 0; i <= w->brush_size; i++) {
    int r = i, f = 1 - r;
    int dx = 0, dy = -2 * r;
    int x0 = w->brush_x, y0 = w->brush_y;
    int x = 0, y = r;

    // draw the 4 corners
    set_from_brush(w, x0, y0+r);
    set_from_brush(w, x0, y0-r);
    set_from_brush(w, x0-r, y0);
    set_from_brush(w, x0+r, y0);

    while (x < y) {
      if (f >= 0) {
	y--;
	dy += 2;
	f += dy;
      }

      x++;
      dx += 2;
      f += dx + 1;

      // draw some more stuff
      set_from_brush(w, x0+x, y0+y);
      set_from_brush(w, x0+x, y0-y);
      set_from_brush(w, x0-x, y0+y);
      set_from_brush(w, x0-x, y0-y);
      set_from_brush(w, x0+y, y0+x);
      set_from_brush(w, x0+y, y0-x);
      set_from_brush(w, x0-y, y0+x);
      set_from_brush(w, x0-y, y0-x);
    }
  }
}


// handle events
void world_hdl_event(world* w, event* e) {
  switch (e->type) {
  case EV_MOUSEMOVED:
    w->brush_x = e->x / PIXEL_SIZE;
    w->brush_y = e->y / PIXEL_SIZE;
    break;

  case EV_SCROLL:
    w->brush_size += e->y;
    if (w->brush_size < 0)
      w->brush_size = 0;
    break;

  case EV_KEYPRESS: {
    keycode kc = e->key.sym;
    
    if (kc == SDLK_d) {
      // next brush
      w->brush_idx =  (w->brush_idx+1) % NUM_BRUSHES;
#ifdef DEBUG
      printf("switch to brush #%d\n", w->brush_idx);
#endif
    } else if (kc == SDLK_a) {
      // prev brush
      w->brush_idx = (w->brush_idx == 0)? NUM_BRUSHES-1 : w->brush_idx-1;
#ifdef DEBUG
      printf("switch to brush #%d\n", w->brush_idx);
#endif
    } else if (kc == SDLK_w) {
      // jump to eraser brush
      w->brush_idx = ERASER_IDX;
#ifdef DEBUG
      printf("switch to brush #%d (eraser)\n", w->brush_idx);
#endif
    }
    
    break;
  }
    
  default: break;
  }
}

// update the world, i.e., simulate the physics and chemistry and psychology of the pixels.
void update_world(world* w) {
  // update the heatmap
  update_heatmap(w);
  
  if (is_left_pressed())
    draw_pixels(w);
  
  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_WIDTH; y++)
      update_pixel(w, x, y);

  // double buffering
  flip(w);
}

// update a singular pixel by moving it, transmuting it, etc.
void update_pixel(world* w, int x, int y) {
  if (!valid_coords(x, y)) return;

  pixel* p = get_pixel(w, x, y);
  uint32_t flags = p->flags;
  uint32_t density = p->density;

  // some of us are saints, some are clowns
  // just like me, they're falling down
  if (flags & PX_FALLS) {
    // try moving the pixel downwards
    bool moved_down = move_pixel(w, x, y, 0, 1, p);
    // if it didn't moved downwards, try moving downwards on the side.
    // this is where things start to get kind of cursed.
    if (!moved_down) {
      if (is_open_spot(w, density, x-1, y-1)
      	  && is_open_spot(w, density, x, y-1)
      	  && is_open_spot(w, density, x+1, y-1)
      	  && is_open_spot(w, density, x-1, y)
      	  && is_open_spot(w, density, x+1, y)
      	  && is_open_spot(w, density, x-1, y+1)
      	  && is_open_spot(w, density, x+1, y+1))
      	move_pixel(w, x, y, chance(2)? -1 : 1, 0, p);
      else if (is_open_spot(w, density, x-1, y-1)
      	       && is_open_spot(w, density, x, y-1)
      	       && is_open_spot(w, density, x-1, y)
      	       && is_open_spot(w, density, x-1, y+1))
      	move_pixel(w, x, y, -1, 0, p);
      else if (is_open_spot(w, density, x, y-1)
      	       && is_open_spot(w, density, x+1, y-1)
      	       && is_open_spot(w, density, x+1, y)
      	       && is_open_spot(w, density, x+1, y+1))
      	move_pixel(w, x, y, 1, 0, p);
      else if (flags & PX_FLOWS) {
      	// flowing pixels like to move from side to side
      	if (chance(2) && is_open_spot(w, 0, x+1, y)) {
      	  move_pixel(w, x, y, 1, 0, p);
      	} else if (is_open_spot(w, 0, x-1, y)) {
      	  move_pixel(w, x, y, -1, 0, p);
      	}
      }
    }
  }

  /* if (p->id != ID_EMPTY) */
  /*   transmute_pixel(p, ID_WATER); */
}

// Try to move a pixel to the specified location. If the move is successful (ie unobstructed),
// return `true`; return `false` if the pixel wasn't able to be moved. Moving a pixel
// basically works by swapping it with the pixel in its new location;
bool move_pixel(world* w, int x, int y, int dx, int dy, pixel* p) {
  int x2 = x+dx;
  int y2 = y+dy;

  if (!is_open_spot(w, p->density, x2, y2)) return false;
  
  pixel* other = get_pixel(w, x2, y2); // openness check above implies this is non-null
  set_pixel(w, x, y, other);
  set_pixel(w, x2, y2, p);

  return true;
}

// Determine whether a location is open for another pixel to move onto it.
// This basically means the following:
// - the location may be empty
// - or it may be displaceable
// - or it's less dense than the pixel being moved
// - and it can't be outside the world
bool is_open_spot(world* w, uint32_t density, int x, int y) {
  if (!valid_coords(x, y)) return false;

  pixel* p = get_pixel(w, x, y);
  pixel* q = w->new_pixels[x][y];
  uint32_t pflags = p->flags;
  uint32_t qflags = q->flags;
  uint32_t pdensity = p->density;
  uint32_t qdensity = q->density;

  bool empty = ((pflags & PX_EMPTY) || (pflags & PX_DISPLACEABLE))
    && ((qflags & PX_EMPTY) || (qflags & PX_DISPLACEABLE));
  bool denser = (density > pdensity) && (density > qdensity);

  return (empty || denser);
}



// gets the heat at the given point, and returns 0.0 if the point is invalid
float get_heatmap(world* w, int x, int y) {
  if (!valid_coords(x, y)) return 0.0f;

  return w->heatmap[x][y];
}

// sets the heat at the given point, and does nothing if the point is invalid
void set_heatmap(world* w, int x, int y, float h) {
  if (!valid_coords(x, y)) return;

  w->heatmap[x][y] = h;
}

void update_heatmap(world* w) {
  float new_heatmap[WORLD_WIDTH][WORLD_HEIGHT];

  // updating the heatmap basically just entails averaging out
  // all the temperature values, taking specific heat capacity
  // of pixel materials into account
  for (int x = 0; x < WORLD_WIDTH; x++) {
    for (int y = 0; y < WORLD_HEIGHT; y++) {
      // calculate the average temperature
      // in the cell's moore neighborhood
      float sum = 0.0f;
      int n = 0;
      for (int u = x-1; u <= x+1; u++) {
	for (int v = y-1; v <= y+1; v++) {
	  if (valid_coords(u, v)) {
	    sum += get_heatmap(w, x, y);
	    n++;
	  }
	}
      }
      
      float avg = (n != 0)? (sum/n) : 0.0f;
      new_heatmap[x][y] = avg;
    }
  }

  for (int x = 0; x < WORLD_WIDTH; x++)
    for (int y = 0; y < WORLD_HEIGHT; y++)
      set_heatmap(w, x, y, new_heatmap[x][y]);
}
