#include "event.h"

// wraps an event into a struct that this module uses
bool wrap_event(SDL_Event sdl_event, event* e) {
  switch (sdl_event.type) {
  case SDL_QUIT:
    e->type = EV_QUIT;
    break;

  case SDL_MOUSEBUTTONUP:
  case SDL_MOUSEBUTTONDOWN:
    e->type = EV_MOUSEPRESS;
    get_mouse_pos(&e->x, &e->y);
    break;

  case SDL_MOUSEMOTION:
    e->type = EV_MOUSEMOVED;
    get_mouse_pos(&e->x, &e->y);
    break;

  case SDL_MOUSEWHEEL:
    e->type = EV_SCROLL;
    e->x = sdl_event.wheel.x;
    e->y = sdl_event.wheel.y;
    break;

  case SDL_KEYUP:
    e->type = EV_KEYPRESS;
    e->key = sdl_event.key.keysym;
    break;
    
  default: return false;
  }

  return true;
}

// returns a new event (NULL if there isn't one)
bool poll_event(event* e) {
  SDL_Event sdl_event;

  return SDL_PollEvent(&sdl_event) && wrap_event(sdl_event, e);
}

// sets `x` and `y` to the current position of the mouse
void get_mouse_pos(int* x, int* y) {
  SDL_GetMouseState(x, y);
}

// returns true if the left mouse button is pressed.
bool is_left_pressed() {
  return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT);
}
