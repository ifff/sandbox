#include <math.h>
#include "../includes.h"
#include "renderer.h"


renderer_info renderer_init(int width, int height) {
  SDL_Window* win = NULL;
  SDL_Renderer* ren;
  renderer_info info;

  // init SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL had an error: %s\n", SDL_GetError());
  }

  SDL_CreateWindowAndRenderer(width, height, 0, &win, &ren);

  info.ren = ren;
  info.win = win;
  info.width = width;
  info.height = height;

#ifdef DEBUG
  printf("initialized renderer.\n");
#endif
  
  return info;
}

void renderer_destroy(renderer_info* info) {  
  SDL_DestroyRenderer(info->ren);
  SDL_DestroyWindow(info->win);
  SDL_Quit();

#ifdef DEBUG
  printf("destroyed renderer.\n");
#endif
}

void renderer_refresh(renderer_info* info) {
  SDL_RenderPresent(info->ren);
}

void renderer_draw_rect(renderer_info* info, int x, int y, int w, int h) {
  SDL_Rect r;
  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;

  SDL_RenderFillRect(info->ren, &r);
}

void renderer_set_color(renderer_info* info, int r, int g, int b, int a) {
  SDL_SetRenderDrawColor(info->ren, r, g, b, a);
}
