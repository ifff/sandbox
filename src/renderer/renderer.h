/**
 * All the rendering functionality needed by other parts of the program.
 */

#ifndef REN_RENDERER_H
#define REN_RENDERER_H

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

typedef struct {
  SDL_Renderer* ren;
  SDL_Window* win;
  int width, height;
} renderer_info;


// simple stuff that deals directly with the renderer
renderer_info renderer_init(int width, int height);
void renderer_destroy(renderer_info* info);
void renderer_refresh(renderer_info* info);
void renderer_draw_rect(renderer_info* info, int x, int y, int w, int h);
void renderer_set_color(renderer_info* info, int r, int g, int b, int a);

#endif
