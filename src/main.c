/**
 * An isometric, minecraft-style game.
 */

#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "includes.h"
#include "renderer/renderer.h"
#include "renderer/event.h"
#include "world/world.h"
#include "graphics/graphics.h"

int main() {
  #ifdef DEBUG
  printf("\n========= DEBUG LOG =========\n");
  #endif
  
  // renderer stuff
  renderer_info ren;
  event e;
  // set up renderer
  ren = renderer_init(SCREEN_WIDTH, SCREEN_HEIGHT);

  srand(time(NULL));

  // set up the world
  world* w = new_world();
  
  // main loop
#ifdef DEBUG
  printf("entering game loop...\n");
#endif
  while (1) {
    // delegate event handling
    if (poll_event(&e)) {
      switch (e.type) {
      case EV_QUIT:
	goto goodbye;
	break;
      default:
	world_hdl_event(w, &e);
      }
    }

    update_world(w);
    draw_world(&ren, w);
    
    renderer_refresh(&ren);
  }
  
 goodbye:
  // burn everything down, tear it to shreds
#ifdef DEBUG
  printf("cleaning up...\n");
#endif

  renderer_destroy(&ren);
  destroy_world(w);
  
  // yay we did it
#ifdef DEBUG
  printf("cleanup successful.\n");
#endif
  return 0;
}
