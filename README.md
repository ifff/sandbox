# sandbox
This is an attempt to replicate an old mobile game called "the sandbox." Of course, there are
zero payments and advertisements in this version ^^.

## how to use
There's no good user interface at this point, so you have to use keys to do things:
* a = previous brush
* w = eraser
* d = next brush
* right click = draw
* scroll = change brush size
